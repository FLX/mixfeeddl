import requests
import re
from bs4 import BeautifulSoup
import sys
import os
import os.path
import datetime
import string
from mutagen.id3 import ID3, TPE1, TALB, TIT2, ID3NoHeaderError

folder = '/Users/dennis/Dropbox/Music/Web/Diplo & Friends/'
headers = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29.0) Gecko/20100101 Firefox/29.0'}

def getShowList(url):

	r = requests.get(url, headers=headers)

	soup = BeautifulSoup(r.text)

	#print(soup.prettify())
	for link in soup("div", { "class":"post-blog" }):
		theShowLink = link.find("a", class_="middle-button").get('href')
		#print theShowLink
		getShowLink(theShowLink)


def getShowLink(url):
	r = requests.get(url, headers=headers)

	soup = BeautifulSoup(r.text)

	theTitle = soup.find("div", { "class":"blog-page" }).find("h1").string.encode('ascii','ignore')
	#print(soup.prettify())
	for link in soup("a", text=re.compile("Diplo and Friends"), href=re.compile("mixcloud")):
		extractedLink = link.get('href')
		theLink = extractedLink.replace("?utm_source=widget&utm_medium=web&utm_campaign=base_links&utm_term=resource_link", "")
		downloadShow(theLink, theTitle)
	

def downloadShow(url,title):
	headers = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29.0) Gecko/20100101 Firefox/29.0', 'Referer': 'http://offliberty.com/'}
	payload = {'track': url, 'refext': 'http://offliberty.com/off54.php'}

	r = requests.post('http://offliberty.com/off54.php', headers=headers, data=payload)
	soup = BeautifulSoup(r.text)
	#print(soup.prettify())

	print "Downloading " + title
	m = re.match(r"^(.*)  (.*) \((.*)\)$", title)
	try:
		artist = m.group(1)
		show = m.group(2)
		date = m.group(3)
		date = re.sub("\(","",date)
		date = re.sub("\)","",date)
		dateObj = datetime.datetime.strptime(date,"%m-%d-%Y")
		dateString = dateObj.strftime("%Y-%m-%d")
		filename = "{0} - {1} - {2}.mp3".format(dateString, show, artist)
		matched = True
	except:
		filename = "{0}.mp3".format(title)
		matched = False

	if (os.path.exists(folder+filename) == False):
		if (soup.find("a", { "class":"download" }).get('href') != None):
			downloadURL = soup.find("a", { "class":"download" }).get('href')
			print "Downloading " + downloadURL
			
			
			os.system("wget -c \"{0}\" -O \"{1}\"".format(downloadURL,folder+filename+".part"))
			os.rename(folder+filename+".part", folder+filename)
			try:
				audio = ID3(folder+filename)
			except ID3NoHeaderError:
				audio = ID3()
				if matched:
					audio.add(TPE1(encoding=3, text=artist))
					audio.add(TIT2(encoding=3, text=dateString + " - " + artist))
					audio.add(TALB(encoding=3, text=show))
				else:
					audio.add(TPE1(encoding=3, text=title))
					audio.add(TIT2(encoding=3, text=title))
					audio.add(TALB(encoding=3, text="Diplo and Friends"))
				audio.save(folder+filename)
	else:
		print title + " already exists, moving on to the next"

if __name__ == '__main__':
	getShowList('http://www.themixfeed.com/category/mixes/diplo-and-friends/')