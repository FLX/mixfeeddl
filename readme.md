# Description
Automatically download shows from http://themixfeed.com

Relies on wget and the following Python packages:

* [Mutagen](https://code.google.com/p/mutagen/): a Python module to handle audio metadata, used for writing ID3 tags
* [BeautifulSoup4](http://www.crummy.com/software/BeautifulSoup/): for scraping http://themixfeed.com
* [Requests](http://docs.python-requests.org/en/latest/): because the API on urllib2 completely sucks

# Installation
- `pip install -r REQUIREMENTS`
- edit `download.py` for output path and shows
- `python download.py`
- sit back and relax
